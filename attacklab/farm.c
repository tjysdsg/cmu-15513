/*
 * Note: To get instruction addresses, DO NOT compile this file yourself.
 * Instead, you should disassemble the existing rtarget binary.
 */

/* This function marks the start of the farm */
int start_farm()
{
    return 1;
}

unsigned getval_106()
{
    return 3284633928U;
}

unsigned addval_497(unsigned x)
{
    return x + 3281031256U;
}

unsigned addval_204(unsigned x)
{
    return x + 3347662962U;
}

unsigned getval_302()
{
    return 3351742792U;
}

void setval_191(unsigned *p)
{
    *p = 2438472816U;
}

unsigned getval_442()
{
    return 3281031256U;
}

unsigned getval_153()
{
    return 2428995944U;
}

unsigned addval_490(unsigned x)
{
    return x + 2425394264U;
}

/* This function marks the middle of the farm */
int mid_farm()
{
    return 1;
}

/* Add two arguments */
long add_xy(long x, long y)
{
    return x+y;
}

void setval_266(unsigned *p)
{
    *p = 3374893705U;
}

unsigned addval_413(unsigned x)
{
    return x + 2361573769U;
}

unsigned addval_296(unsigned x)
{
    return x + 3380926088U;
}

unsigned addval_170(unsigned x)
{
    return x + 3531915657U;
}

void setval_185(unsigned *p)
{
    *p = 2497743176U;
}

unsigned addval_262(unsigned x)
{
    return x + 3247491721U;
}

void setval_353(unsigned *p)
{
    *p = 3286272328U;
}

void setval_263(unsigned *p)
{
    *p = 2497743176U;
}

void setval_198(unsigned *p)
{
    *p = 3281109385U;
}

unsigned addval_368(unsigned x)
{
    return x + 3682915977U;
}

unsigned addval_408(unsigned x)
{
    return x + 3284830591U;
}

unsigned getval_130()
{
    return 3224945033U;
}

unsigned getval_405()
{
    return 2425405897U;
}

unsigned getval_288()
{
    return 2464188744U;
}

unsigned addval_337(unsigned x)
{
    return x + 2429979565U;
}

unsigned getval_151()
{
    return 3232025225U;
}

unsigned addval_363(unsigned x)
{
    return x + 3281305993U;
}

void setval_148(unsigned *p)
{
    *p = 3286272328U;
}

unsigned addval_389(unsigned x)
{
    return x + 3676359297U;
}

unsigned getval_377()
{
    return 2462157292U;
}

void setval_345(unsigned *p)
{
    *p = 3286272320U;
}

unsigned getval_217()
{
    return 3353381192U;
}

unsigned addval_136(unsigned x)
{
    return x + 3285614915U;
}

void setval_147(unsigned *p)
{
    *p = 3398027512U;
}

unsigned addval_206(unsigned x)
{
    return x + 3221799321U;
}

unsigned addval_210(unsigned x)
{
    return x + 3229931209U;
}

unsigned getval_284()
{
    return 3389592362U;
}

unsigned getval_253()
{
    return 3281049225U;
}

unsigned addval_424(unsigned x)
{
    return x + 2430634248U;
}

unsigned getval_109()
{
    return 3599599219U;
}

unsigned getval_146()
{
    return 3375945345U;
}

void setval_265(unsigned *p)
{
    *p = 3526937097U;
}

/* This function marks the end of the farm */
int end_farm()
{
    return 1;
}
