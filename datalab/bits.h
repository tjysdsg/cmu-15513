//1
long bitMatch(long, long);
long test_bitMatch(long, long);
//2
long leastBitPos(long);
long test_leastBitPos(long);
long dividePower2(long, long);
long test_dividePower2(long, long);
long implication(long, long);
long test_implication(long, long);
long oddBits(void);
long test_oddBits(void);
//3
long rotateLeft(long, long);
long test_rotateLeft(long, long);
long isLess(long, long);
long test_isLess(long, long);
//4
long leftBitCount(long);
long test_leftBitCount(long);
long integerLog2(long);
long test_integerLog2(long);
long trueThreeFourths(long);
long test_trueThreeFourths(long);
long howManyBits(long);
long test_howManyBits(long);
//float
int floatIsEqual(unsigned, unsigned);
int test_floatIsEqual(unsigned, unsigned);
unsigned floatScale2(unsigned);
unsigned test_floatScale2(unsigned);
unsigned floatUnsigned2Float(unsigned);
unsigned test_floatUnsigned2Float(unsigned);
